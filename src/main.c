#include "mem.h"


int main(){
    heap_init(1000);

    //Последовательное выделение памяти
    void * a = _malloc(200);
    void * b = _malloc(300);
    void * c = _malloc(400);
    debug_heap(stdout, HEAP_START);

    // free
    _free(b);
    debug_heap(stdout, HEAP_START);

    //Выделение памяти в середине
    b = _malloc(30);
    debug_heap(stdout, HEAP_START);

    //merge освободившихся блоков
    _free(b);
    _free(a);
    debug_heap(stdout, HEAP_START);
    

    //Выделение новой памяти
    void *x = _malloc(10000);
    debug_heap(stdout, HEAP_START);


    
    _free(x);
    _free(c);


}
